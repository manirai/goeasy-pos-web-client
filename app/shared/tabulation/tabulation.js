/**
 * 
 */
var tabulationApp = angular.module('tabulationApp', [ 'ngGrid' ]);
tabulationApp
		.controller(
				'tabulationController',
				function($scope) {
					$scope.tabulationData = [];
					$scope.tabulationGrid = {
						data : 'tabulationData',
						enableCellSelection : true,
						enableCellEditOnFocus : true,
						enableRowSelection : false,
						enableCellSelection : false,
						columnDefs : [
								{
									field : 'serialNo',
									displayName : 'S.NO',
									width : 50
								},
								{
									field : 'item',
									displayName : 'ITEM'
								},
								{
									field : 'quantity',
									displayName : 'QUANTITY',
									width : 100
								},
								{
									field : 'price',
									displayName : 'PRICE',
									width : 150
								},
								{
									width : 27,
									cellTemplate : '<span class="glyphicon glyphicon-remove tabulation-row-remove" ng-click="removeRow()"></span>',
									enableCellEdit : false
								} ]
					};

					$scope.addRow = function() {
						$scope.tabulationData[$scope.tabulationData.length] = {
							serialNo : $scope.tabulationData.length + 1,
							item : $scope.item,
							quantity : $scope.quantity,
							price : 4999
						};
					};

					$scope.removeRow = function() {
						$scope.tabulationData.splice(this.row.rowIndex, 1);
						for (var index = this.row.rowIndex; index < $scope.tabulationData.length; index++) {
							$scope.tabulationData[index].serialNo = index + 1;
						}
					};
				});